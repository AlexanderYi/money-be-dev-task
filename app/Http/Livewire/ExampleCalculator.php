<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ExampleCalculator extends Component
{
    public const DURATION_ONE_YEAR = 1;

    public const DURATION_THREE_YEARS = 3;

    public const DURATION_FIVE_YEARS = 5;

    public const DURATIONS = [
        self::DURATION_ONE_YEAR,
        self::DURATION_THREE_YEARS,
        self::DURATION_FIVE_YEARS,
    ];

    public const FREQUENCY_MONTH = 'Month';

    public const FREQUENCY_FORTNIGHT = 'Fortnight';

    public const FREQUENCY_WEEK = 'Week';

    public const FREQUENCIES = [
        self::FREQUENCY_MONTH,
        self::FREQUENCY_FORTNIGHT,
        self::FREQUENCY_WEEK,
    ];

    public const FREQUENCY_CONVERSION = [
        self::FREQUENCY_MONTH => 12,
        self::FREQUENCY_FORTNIGHT => 26,
        self::FREQUENCY_WEEK => 52,
    ];

    public float $result = 0.00;

    public $loanAmount;

    public $interestRate;

    public $termDuration;

    public string $frequency = self::FREQUENCY_MONTH;

    public array $durations = self::DURATIONS;

    public array $frequencies = self::FREQUENCIES;

    public function mount()
    {
        // So far, nothing to do here.
    }

    public function calculateRate()
    {
        // Normally, minus values of amount and interest rate are not acceptable
        $this->loanAmount = max(0, (float) ($this->loanAmount ?? 0));
        $this->interestRate = max(0, (float) ($this->interestRate ?? 0));

        // Validate the term duration, default to one year
        $this->termDuration = (int) ($this->termDuration ?? self::DURATION_ONE_YEAR);
        if (!in_array($this->termDuration, self::DURATIONS, true)) {
            $this->termDuration = self::DURATION_ONE_YEAR;
        }
        
        // Validate the frequency, default to month
        if (!in_array($this->frequency, array_keys(self::FREQUENCY_CONVERSION), true)) {
            $this->frequency = self::FREQUENCY_MONTH;
        }

        /**
         * A = P(1 + rt)
         * 
         * Where:
         *     A = Total Accrued Amount (principal + interest)
         *     P = Principal Amount; it is $this->loanAmount
         *     R = Rate of Interest per year as a percent; it is $this->interestRate
         *     t = Time Period involved in years; it is $this->termDuration
         *     r = Rate of Interest per year in decimal; r = R/100
         */
        $totalAmount = $this->loanAmount * (1 + $this->interestRate * $this->termDuration / 100);

        /**
         * Repayment period = years * terms (how many in a year)
         */
        $repaymentPeriod = $this->termDuration * self::FREQUENCY_CONVERSION[$this->frequency];

        /**
         * Result = Total Amount / Repayment Period
         */
        $this->result = $totalAmount / $repaymentPeriod;
    }

    public function changeFrequency(string $frequency): void
    {
        if (in_array($frequency, self::FREQUENCIES, true)) {
            $this->frequency = $frequency;
        } else {
            $this->frequency = self::FREQUENCY_MONTH;
        }

        // The change of frequency will influence the result
        $this->calculateRate();
    }

    public function updated(): void
    {
        // Any changes will influence the result
        $this->calculateRate();
    }

    public function render()
    {
        return view('livewire.example-calculator');
    }
}
