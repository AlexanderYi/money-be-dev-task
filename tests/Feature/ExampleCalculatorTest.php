<?php

namespace Tests\Feature;

use App\Http\Livewire\ExampleCalculator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class ExampleCalculatorTest extends TestCase
{
    public function testBasicResponse(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee(['Monthly', 'Fortnightly', 'Weekly', '1 Year', '3 Years', '5 Years']);
    }

    public function frequencyProvider(): iterable
    {
        yield [ExampleCalculator::FREQUENCY_MONTH, ExampleCalculator::FREQUENCY_MONTH];
        yield [ExampleCalculator::FREQUENCY_FORTNIGHT, ExampleCalculator::FREQUENCY_FORTNIGHT];
        yield [ExampleCalculator::FREQUENCY_WEEK, ExampleCalculator::FREQUENCY_WEEK];
        // Wrong frequency input will fallback to default
        yield ['something wrong happened so that the frequency will be set as Month', ExampleCalculator::FREQUENCY_MONTH];
    }

    /**
     * @dataProvider frequencyProvider
     *
     * @return void
     */
    public function testChangeFrequency(string $frequency, string $expected): void
    {
        Livewire::test(ExampleCalculator::class)
            ->call('changeFrequency', $frequency)
            ->assertSet('frequency', $expected);
    }

    public function calculateRateProvider(): iterable
    {
        // Invalid input will fallback to default
        yield [
            [
                'loanAmount' => null,
                'interestRate' => null,
                'termDuration' => null,
                'frequency' => '',
            ],
            [
                'loanAmount' => 0,
                'interestRate' => 0,
                'termDuration' => ExampleCalculator::DURATION_ONE_YEAR,
                'frequency' => ExampleCalculator::FREQUENCY_MONTH,
                'result' => 0,
            ]
        ];

        yield [
            [
                'loanAmount' => -1,
                'interestRate' => -1,
                'termDuration' => 2,
                'frequency' => '',
            ],
            [
                'loanAmount' => 0,
                'interestRate' => 0,
                'termDuration' => ExampleCalculator::DURATION_ONE_YEAR,
                'frequency' => ExampleCalculator::FREQUENCY_MONTH,
                'result' => 0,
            ]
        ];

        // Normal calculation
        yield [
            [
                'loanAmount' => 120,
                'interestRate' => 10,
                'termDuration' => ExampleCalculator::DURATION_THREE_YEARS,
                'frequency' => ExampleCalculator::FREQUENCY_WEEK,
            ],
            [
                'loanAmount' => 120,
                'interestRate' => 10,
                'termDuration' => ExampleCalculator::DURATION_THREE_YEARS,
                'frequency' => ExampleCalculator::FREQUENCY_WEEK,
                'result' => 1,
            ]
        ];

        yield [
            [
                'loanAmount' => 100,
                'interestRate' => 6,
                'termDuration' => ExampleCalculator::DURATION_FIVE_YEARS,
                'frequency' => ExampleCalculator::FREQUENCY_FORTNIGHT,
            ],
            [
                'loanAmount' => 100,
                'interestRate' => 6,
                'termDuration' => ExampleCalculator::DURATION_FIVE_YEARS,
                'frequency' => ExampleCalculator::FREQUENCY_FORTNIGHT,
                'result' => 1,
            ]
        ];
    }

    /**
     * @dataProvider calculateRateProvider
     *
    * @return void
     */
    public function testCalculateRate(array $input, array $expected): void
    {
        Livewire::test(ExampleCalculator::class)
            ->set('loanAmount', $input['loanAmount'] ?? null)
            ->set('interestRate', $input['interestRate'] ?? null)
            ->set('termDuration', $input['termDuration'] ?? null)
            ->set('frequency', $input['frequency'] ?? null)
            ->call('calculateRate')
            ->assertSet('loanAmount', $expected['loanAmount'] ?? null)
            ->assertSet('interestRate', $expected['interestRate'] ?? null)
            ->assertSet('termDuration', $expected['termDuration'] ?? null)
            ->assertSet('frequency', $expected['frequency'] ?? null)
            ->assertSet('result', $expected['result'] ?? null);
    }
}
